---
title: About my blog
permalink: /programming/about-my-blog.html
description: The technologies and workflow behind this blog.
date: 2020-05-29
---

## Choosing a Static Site Generator

When I first decided I wanted to make this site, I really just wanted it to be as low maintenance as possible and have a simple workflow for adding new content to it. After looking around a little bit I decided on using a SSG which would allow me to add content just by writing new markdown files, and deploy it quickly just by commiting it. I originally settled on learning [Hugo](https://gohugo.io) since Go is my preferred language at the moment. After messing around with it for a week or two, it just wasn't clicking with me. I started to look around again and was about to just use [Gatsby](https://gatsbyjs.org) since i've used it on a project before and i'm very familiar with react, despite it being a bit overkill for this situation. Just before choosing though I found [11ty](https://11ty.dev). It looked promising and I started to mess around with it with mixed results. I wasn't able to quite get it working right from just the docs so I turned to external tutorials instead. This is now the second time i've been saved by one of Jason Lengstorf and one of his [Learn with Jason](https://www.learnwithjason.dev/) videos. In this video he works with Zach Leatherman (I think he's the creator of eleventy, or at least part of the project), and goes from not knowing anything to building a site with the help of Zach. This video made everything click for me and from there I was on my way.

## Building the site

Like most SSGs, 11ty uses templates that are then filled in with the content from markdown files. I created a base template that takes care of all the HTML boilerplate and also takes care of importing and minifying all of the syles being used along with all the fonts. The only stylesheet being minified is Normalize.css, and while I could have just used the minified version, this gave me a good opportunity to write a filter. A filter in 11ty is basically a javascript function that can be called from within a template and return data back to the template. In this case I just created a filter called cssmin that takes a parameter, minifies it, then returns the minified string. The whole function looks like this:

```javascript
eleventyConfig.addFilter("cssmin", function (code) {
  return new CleanCSS({}).minify(code).styles;
});
```

eleventyConfig is an object provided by the framework that lets you add in filters and a bunch of other stuff. While writing this I went to the docs to try and find documentation on everything you could do with this object which but couldn't find anything about it. I did notice while trying to figure out the site that the documentation isn't the best so some things you have to figure out by looking at external tutorials, or if you really want to, reading the source.

Overall building the site was pretty pleasant once I got a hang of using 11ty, and i'm sure I haven't even scratched the surface of how much you can do with the framework. If you're interested in looking at the code for the site it can be found [here](https://gitlab.com/sean.manierre/blog).

## The Deployment Pipeline

Going along the lines of having it be easy to create content for the blog, I also wanted it to be easy to update. Since this project isn't really important and easy to build, I figured it would be a good way to get familiar with Gitlabs CI/CD pipeline. Since the project is so small and there isn't much in the file, which can be seen [here](https://gitlab.com/sean.manierre/blog/-/blob/master/.gitlab-ci.yml).

It's based on a docker image with the current version of node that I have on my laptop. It first runs the `before_script` section which sets up the ssh key for my server and installs rsync which I use to copy over the created website. Once the setup is complete it installs the dependencies needed for 11ty, compiles the SCSS, and builds the site. After building it uses ssh to back up the existing site in a different directory on the server, and then copies the newly built files onto the server to be served up by Nginx.
