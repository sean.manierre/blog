---
title: Golang Type Site
permalink: /programming/golang-type-site.html
description: A quick post about my personal Golang project.
date: 2020-06-26
---

## The Problem

A few months ago I started to look for a new backend language to use since Java + Spring Boot just wasn't clicking for me.
I looked at C++ shortly since I used that in college a bit and liked it but after seeing people post about how big the language
had become and how many different styles there are I got scared off a bit. In comes everyones favorite [gopher](https://blog.golang.org/gopher).
After playing around with Go, I quickly fell in love with how there was usually only one proper way to solve some issues and how simple the language was.
Even concurrency, which can be very tricky in most languages, is made fairly simple with channels and the `select` statement.
Another feature I like, and where the problem started, is implicit interfaces. Some languages, like Java, require interfaces to be implemented explicitly like so:

```java
    public class IThing implements OtherThing, OtherOtherThing, OtherOtherThingFactoryThing {}
```

In Go however, interfaces are implemented implicitly. Take the following code for example.

```golang
    type Iface interface {
        DoThing() int
        DoOtherThing(int) error
    }

    type Thing struct{}

    func (t Thing) DoThing() int {return -1}
    func (t Thing) DoOtherThing(i int) error {return nil}

    func AcceptInterface(i Iface) {}
```

The interface `Iface` defines two methods `DoThing` which has no parameters and returns an `integer`, and `DoOtherThing` which takes an `integer` and returns an `error`, which is itself an interface.
We then define the `Thing` struct. For the sake of it not being cluttered, there are no fields in the struct, and the methods don't actually do anything.
Below the struct we define two methods `DoThing` and `DoOtherThing` on the struct. The syntax is a bit different than other languages but once you get used to it it's pretty nice.
Now since the `Thing` struct has methods attached to it that satisfy all the requirements of the `Iface` interface, it can be used anywhere an `Iface` is called for, such as `AcceptInterface`, without having to explicitly state that `Thing` implements `Iface`.
While this is a really nice feature that I like most of the time, it's hard to figure out what types can be used in certain places when an interface is accepted.
To help with this problem, I decided to create [satisfies.it](https://satisfies.it).

## Choosing the Stack

First and foremost, the repository can be found [here](https://gitlab.com/sean.manierre/typer-site), if you feel like looking at some sub-par code.

Since this is my first major project using Go, I wanted to stick to using as few libraries as possible and just use the standard library (which is amazing btw) for as much as possible.
At the time of writing this, and with the app mostly done, I only have 2 libraries in use. The first is [godotenv](https://github.com/joho/godotenv) which is used to read in values from a .env file to be used in case parameters aren't passed in through the command line.
The other library is [pq](https://github.com/lib/pq) which is a driver for using a Postgres database. I recently found out that the creator of pq has said to move to [pgx](https://github.com/jackc/pgx), so i'll probably switch over to that once I get the application up and running. Besides those two libraries, the rest of the app uses only the standard library and the `net/http` package for all the routing and API stuff. As mentioned before i'm using a [Postgres](https://postgresql.org) database that i'm running in a docker container so the system being used as the server doesn't need to have a local installation setup. For the frontend i'm using [React](https://reactjs.org) written in [Typescript](https://typescriptlang.org), [React Router](https://reacttraining.com/react-router) for client side routing, and [Styled Components](https://styled-components.com) for all of the CSS.

## How it works

The real meat of the application is in the backend, and actually only run at build time. The `parser` package parses `.go` files and extracts any exported (public) types, interfaces, or methods from them.
It then takes all of the extracted interfaces and looks to see what type implements what interface, and vice versa and writes all that data to a json file to be used later.
My first attempt at parsing all the files was to read each file in character by character and try to parse it based on what was being read in. I quickly realized this method while maybe possible, would not be fun, or quick, to implement.
My next attempt involved everyones favorite ~~unreadable mess~~ tool, Regular Expressions. This worked a bit better than reading in files byte by byte, and while I was getting it to work for the most part, there were so many different edge cases to capture it would either be impossible, or require an absolutely huge regex to capture all of them.
After posting on the [Golang Reddit](https://reddit.com/r/golang), I was made aware of the existence of the `go/parser` and `go/ast` packages in the standard library.
To my joy, it took care of all the parsing for me and returned an AST representation of the file. After some trial and error, I was able to pretty quickly determine how to use the parsed file to extract only the information I needed.
It basically came down to the following procedure: 1. Look for a type or interface definition 2. Get information about the given type/interface such as name, package, basetype, etc. 3. Parse any methods on the type using the following steps: 1. Get the name of the function. 2. Figure out the types of the parameters. 3. Figure out the types of the return values.

I ran into some trouble when attempting to parse the parameters and return values of the functions. Originally I just used nested switch statements to figure out what the current type was and where to go from there.
For example if the current token was `[]` to indicate a slice, I would then check to see what the type of the slice was. This worked at first, but quickly grew into a massive switch statement with cases going all over the place.
All the regular cases like `[]int` or `interface{}` were handled, but I kept running into obscure cases like `[][]int` or `[]*int`, and I knew that whenever the standard library was updated, there was a chance that a new case would be added that I wouldn't be handling already.
I could just keep adding cases as they came up, but ain't nobody got time for that so I decided to think of a new solution. What I came up with was kind of like a string builder, except everytime it parsed a layer of a type, it would append it to a string that was a field on the type, and call a recursive method on the next type.
For example if there was a parameter with a type `[][]*int`, the first call to the function would parse the first token, determine it's a slice, append "[]" to the internal string, then call the same method on the type of the slice.
This would then append "[]" again and call the method on the next type and so on and so forth. This works much better because now matter how complex the type is, i.e. `[][]*[]***int` (if that's even valid), it just works it's way through until it reaches a certain node that is known to be the end of a type.
The only way this will need to be extended is if a new type is added in, but even then it's just one case added to a switch statement to cover every possible usage of that type, which i'd say is pretty sustainable. That got a bit ramble-y, but that is how the "secret sauce" of the app works, the rest is just usual database interactions and API routes.
Thank you for coming to my TED Talk.
