module.exports = () => {
  let age = new Date().getFullYear() - 1995;
  const month = new Date().getMonth();
  const day = new Date().getDate();
  if (!(month >= 7 && day >= 13)) {
    age -= 1;
  }
  return { age };
};
