---
title: Miscellaneous
layout: list.njk
tags: "category"
---

{%- for post in collections.miscellaneous | reverse %}

<li class="main__post-item">
<a href="{{ post.url }}">
<h2 class="main__post-title">{{ post.data.title }}</h2>
<h3 class="main__post-date">{{ post.data.date.toDateString() }}</h3>
<p class="main__post-text">{{ post.data.description }}</p>
</a>
</li>
{%- endfor %}
