---
title: My Homelab Setup
permalink: /homelab/homelab-setup.html
description: A quick post about my homelab setup as of May 2020. I'm hoping to have a much more comprehensive setup in the future, but for now this does just fine.
date: 2020-05-28
---

# The Router

Originally I was running some cheap TP link router that is just plug and play. It worked fine for my needs for a year or so, but I started to notice its limitations when I went to setup a Pi-Hole for the first time. The router wouldn't let me point to the Pi-Hole as the DNS server for the network unless I disabled DHCP and gave every device a static IP. I really don't remember exactly what was happening since this was probably a year ago now, but I either had to choose between having DHCP or a Pi-Hole, and DHCP ended up winning. I looked into putting custom firmware on the router but of course, I bought one of the only models whose chipset wasn't supported by any of the custom firmwares.

Enter PfSense. I found a cheap HP T620 Plus on Ebay and grabbed it to use as a dedicated PfSense box. I read that these work great since they are quiet, low power, and have a quad port NIC. Just make sure to get one with an Intel NIC as a lot of people say the Realtek NICs don't always play nice with PfSense. I was a bit surprised at how big it was, but it's sitting kind of behind my couch so no one will really see it. It only took about 2 hours on a Saturday morning to get it setup and routing properly. I ran into a couple issues where everything on the network could talk, but I couldn't get out to the WAN, despite being assigned a public IP. It ended up being I specified what I thought was the default gateway for my LAN, but was actually the gateway the router itself was using. So any request I sent it would just look to itself for routing and get lost from there. At least that's what I think happened. All I know is I removed the ip I put in for the gateway and everything started working. I had to setup some firewall rules to allow my 4G hotspot to communicate properly, luckily the manufacturer specifies exactly which ports to open up on the router which made it easy.
<br />

# The Network

My network so far is pretty small, although I am proud of how well it works for not having any experience with networking before this. PfSense is at the heart of it as described in the previous post, and one lonely cable is going from there to a D-Link DGS-1210-10P 10 port PoE switch. I went with a PoE since I got a couple of Cisco wireless access points for a steal from [r/HomelabSales](https://reddit.com/r/homelabsales) that I needed to replace the wireless portion of my original router. I'm not using any of the advanced features of the switch yet since I don't have many devices on there, but once I get a larger amount of IoT devices, i'll start making use of different vLANs, althought it'll most likely be through PfSense instead of the switch. If that's how it works that is, I really don't know anything about vLANs at all yet.
<br />

# The Machines

## Main Server

The main "server" in my setup is a 2014(?)ish Mac Mini with a Core 2 Duo and a whopping 6GB of RAM running Ubuntu Server 20.04. It replaced a Raspberry Pi as my main box, and I gotta say it's much nice to have most of my stuff running on an x86 machine instead of ARM. This machine currently runs the following services:

- [Nginx](https://nginx.org)
- [Wireguard](https://wireguard.com)
- [This blog](https://blog.seanmanierre.tech)
- [satisfies.it](https://satisfies.it) (My pet golang project. _May not be done by the time this is posted_)

The Nginx setup is pretty straight forwards. It just serves this blog as a static site straight off the server using a lets encrypt certificate. Based on the certbot test auto-renew _should_ work, but I won't find that out for another 90 days. The [satisfies.it](https://satisfies.it) site is hosted in a docker container so I just have Nginx forward the connection to that container and the server on there handles all the ssl negotiation.

Wireguard is something I wanted to setup for a while and it definitely took me way too long to do because it is amazing. The setup of it was pretty easy once the structure of the configuration file clicked and now I have a super secure VPN into my homelab as long as I have an internet connection that I can toggle with a single command on my laptop.
<br />

## Pi-Hole

Originally I had the Mini running this as I was having issues flashing a fresh copy of raspbian onto the raspberry pi. Once I was able to get a fresh copy of raspbian flashed, I installed Pi-Hole and updated the DNS server in PfSense to point to the raspberry pi instead of the main server. I waited a day for all the DHCP leases to refresh with the new DNS server settings and then uninstalled Pi-Hole from the Mini. I'm just using the default blocklists that come with Pi-Hole and average about 25% of requests getting blocked.
<br />

## NAS

I have one more raspberry pi that currently isn't doing anything, but will soon be given a fresh copy of raspbian and function as a NAS of sorts. I have a 1TB USB drive I keep all my movies and tv shows on so i'll be attaching that as a Samba share and sharing it out to the network. I plan on having an actual NAS at some point with drives in a RAID configuration, but that'll probably wait until I get a house and can setup and actual rack.
